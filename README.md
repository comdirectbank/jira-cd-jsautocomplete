# jira-cd-jsautocomplete

Since Jira 8 I miss the Javascript autocomplete.

This app provides an autocompleter in contexts `jira.general` and `jira.admin`.  

It uses https://github.com/comdirect/jQuery-autoComplete/tree/master-cd, 
which is a fork of https://github.com/Pixabay/jQuery-autoComplete.
